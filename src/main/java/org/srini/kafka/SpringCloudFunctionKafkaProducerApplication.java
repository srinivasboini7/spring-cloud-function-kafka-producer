package org.srini.kafka;

import ch.qos.logback.core.testUtil.RandomUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.EnableKafka;

import java.util.concurrent.ExecutionException;
import java.util.function.Function;

@SpringBootApplication
@EnableKafka
@RequiredArgsConstructor
public class SpringCloudFunctionKafkaProducerApplication {

	private final KafkaProducer<String,String> kafkaProducer ;

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudFunctionKafkaProducerApplication.class, args);
	}


	@Bean
	public Function<String, String> produce(){

		return  s -> {
			try {
				System.out.println("PROCESSING ----> " + s);
				var strKey = RandomStringUtils.randomAlphabetic(10) ;
				var strValue = RandomStringUtils.randomAlphabetic(100) ;

				return kafkaProducer.send(new ProducerRecord<>(s, strKey, strValue)).get().toString();
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			} catch (ExecutionException e) {
				throw new RuntimeException(e);
			}
		};
	}

}
